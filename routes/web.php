<?php


Route::namespace('user')->group(function () {

    Route::get('/', 'homeController@index')->name('user.home');

    // Route::group(['prefix' => 'travel', 'as' => 'travel.'], function () {
    //     Route::get('/', 'travelController@index')->name('index');


    // });

    // Route::group(['prefix' => 'pesan', 'as' => 'pesan.'], function () {
    //     Route::get('/', 'pesanController@index')->name('index');


    // });
});

Auth::routes();

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');

Route::namespace('admin')->middleware('auth:admin')->group(function () {

    Route::group(['prefix' => 'admin','as' => 'admin.'], function(){
        Route::get('/', 'homeController@index')->name('home');
        
    });

    Route::group(['prefix' => 'profile','as' => 'profile.'], function(){
        Route::get('/', 'profileController@index')->name('index');
        Route::post('{profile}/update', 'profileController@update')->name('update');
        
    });

    Route::group(['prefix' => 'soal','as' => 'soal.'], function(){
        Route::get('/', 'soalController@index')->name('index');
        Route::get('/pertanyaan/{id}', 'soalController@edit')->name('pertanyaanIndex');
        Route::post('/pertanyaanUpdate/{id}', 'soalController@update')->name('pertanyaanUpdate');
    });

    // Route::group(['prefix' => 'admin', 'as' => 'galeryAdmin.'], function () {

    //     Route::get('/', 'galeryController@index')->name('index');
    //     Route::post('/store', 'galeryController@store')->name('store');
    //     Route::post('{galeryAdmin}/update', 'galeryController@update')->name('update');
    //     Route::post('{galeryAdmin}/delete', 'galeryController@delete')->name('delete');


    // });
});
Route::get('/home', 'HomeController@index')->name('home');
