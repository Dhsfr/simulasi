<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model{
    protected $fillable=[
        'nama','created_at', 'updated_at'
    ];

    public function pertanyaan(){
        return $this->hasMany('App\Model\Pertanyaan', 'idMapel', 'id');
    }
}
