<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model{
    protected $fillable=[
        'idMapel',
        'pertanyaan',
        'idPilgan',
        'created_at', 
        'updated_at'
    ];

    public function mapel(){
        return $this->belongsTo('App\Model\Mapel', 'idMapel', 'id');
    }

    public function pilgan(){
        return $this->belongsTo('App\Model\pilihanGanda', 'idPilgan', 'id');
    }
}
