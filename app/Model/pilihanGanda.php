<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class pilihanGanda extends Model{
    public $timestamps = false;
    protected $fillable=[
        'A',
        'B',
        'C',
        'D',
        'created_at', 
        'updated_at'
    ];

    public function pertanyaan(){
        return $this->hasMany('App\Model\Pertanyaan', 'idPilgan', 'id');
    }
}
