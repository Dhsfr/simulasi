<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model{
    public $timestamps = false;
    
	protected $fillable = [
		'name', 'created_at', 'updated_at'
	];

    public function getUserObject(){
        return $this->belongsToMany(User::class)->using(UserRole::class);
    }
}
