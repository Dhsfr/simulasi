<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;


$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => Hash::make('secret'),
        'remember_token' => Str::random(10),
    ];
});

$factory->afterCreatingState(User::class, 'admin', function ($user) {
    $user->assignRole('admin');
});

$factory->afterCreatingState(User::class, 'guru', function ($user) {
    $user->assignRole('guru');
});

$factory->afterCreatingState(User::class, 'siswa', function ($user) {
    $user->assignRole('siswa');
});
