<?php

use Illuminate\Database\Seeder;
use App\Model\Pertanyaan;

class PertanyaanTableSeeder extends Seeder{
 
    public function run(){
        for($i=0;$i<50;$i++){
            Pertanyaan::create([
                'idMapel' => 1,
                'pertanyaan' => '',
                'idPilgan' => 1,
                'kunciJawaban' => ''
            ]);

            Pertanyaan::create([
                'idMapel' => 2,
                'pertanyaan' => '',
                'idPilgan' => 1,
                'kunciJawaban' => ''
            ]);

            Pertanyaan::create([
                'idMapel' => 3,
                'pertanyaan' => '',
                'idPilgan' => 1,
                'kunciJawaban' => ''
            ]);

            Pertanyaan::create([
                'idMapel' => 4,
                'pertanyaan' => '',
                'idPilgan' => 1,
                'kunciJawaban' => ''
            ]);
        }
    }
}
