<?php

use App\User;
use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder{

    public function run(){
        factory(User::class)->state('admin')->create([
            'username' => '12345678',
        ]);

        factory(User::class)->state('guru')->create([
            'username' => '1125211511',
        ]);

        factory(User::class)->state('siswa')->create([
            'username' => '87654321',
        ]);
        
       
    }
}
