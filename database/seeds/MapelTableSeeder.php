<?php

use Illuminate\Database\Seeder;
use App\Model\Mapel;

class MapelTableSeeder extends Seeder{
 
    public function run(){
        Mapel::create(['nama' => 'B.indonesia']);
        Mapel::create(['nama' => 'MTK']);
        Mapel::create(['nama' => 'B.inggris']);
        Mapel::create(['nama' => 'IPA']);
    }
}
