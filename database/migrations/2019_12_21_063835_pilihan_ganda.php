<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PilihanGanda extends Migration{
  
    public function up(){
        Schema::create('pilihan_gandas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('A')->nullable();
            $table->longText('B')->nullable();
            $table->longText('C')->nullable();
            $table->longText('D')->nullable();

        });
    }

    public function down(){
        Schema::dropIfExists('pilihanGandas');
    }
}
