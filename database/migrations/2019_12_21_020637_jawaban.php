<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Jawaban extends Migration{
  
    public function up(){
        Schema::create('jawabans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('idPertanyaan');
            $table->string('nama');
            $table->string('jawaban');
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('jawabans');
    }
}
