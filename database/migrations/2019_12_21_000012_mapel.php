<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mapel extends Migration{
 
    public function up(){
        Schema::create('mapels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->rememberToken();
            $table->timestamps();
        });
    }


    public function down(){
        Schema::table('mapels', function (Blueprint $table) {
            
        });
    }
}
