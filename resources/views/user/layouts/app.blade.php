<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="icon" href="../../../../favicon.ico">
	<title>SMP NEGERI 5 KALIBARU SATU ATAP</title>
	<link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('assets/css/carousel.css')}}" rel="stylesheet">
</head>
@yield('extrahead')
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
			<img src="assets/images/ptpnxi1.png" alt="tambah surat" height="40px" align="left"/><a class="navbar-brand" href="#">SMP NEGERI 5 KALIBARU SATU ATAP</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"></li>
				</ul>
				<ul class="navbar-nav px-3">
					<li class="nav-item">
						<a class="nav-link" href="#">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{ route('login') }}">Login</a>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<main role="main">
		@yield('content')
		<footer class="container">
			<p class="float-right"><a href="#">Back to top</a></p>
			<p>&copy; SMP NEGERI 5 KALIBARU SATU ATAP</p>
		</footer>
	</main>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script>window.jQuery || document.write('<script src="{{asset('assets/js/vendor/jquery-slim.min.js')}}"><\/script>')</script>
	<script src="{{asset('assets/js/vendor/popper.min.js')}}"></script>
	<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
	<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
	<script src="{{asset('assets/js/vendor/holder.min.js')}}"></script>
</body>
</html>