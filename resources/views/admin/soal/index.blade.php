@extends('admin.layouts.app')
@section('content')
@section('soal', 'active')
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-primary">
							<h4 class="card-title ">Simple Table</h4>
							<p class="card-category"> Here is a subtitle for this table</p>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table">
									<thead class=" text-primary">
										<th>No</th>
										<th>Nama Mapel</th>
										<th>Action</th>
									</thead>
									<tbody>
										@foreach($soal as $soals)
											<tr>
												<td>{{ $loop->iteration }}</td>
												<td>{{ $soals->nama }}</td>
												<td>
													<a href="{{ route('soal.pertanyaanIndex', $soals->id) }}" class="btn btn-info btn-fab btn-fab-mini btn-round"><i class="material-icons">create</i></a>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
