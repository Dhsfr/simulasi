@extends('admin.layouts.app')
@section('content')
@section('soal', 'active')
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header card-header-primary">
						<h4 class="card-title">{{ $pertanyaan->mapel()->first()->nama }}</h4>
					</div><br><br>
					<div class="card-body">
						<form role="form" action="{{ route('soal.pertanyaanUpdate', $pertanyaan->idMapel) }}" method="POST">
						{{ csrf_field() }}
							<div id="looping">
								@for($i=0; $i<50; $i++)
								<div class="row">
									<div class="col-md-12">
										<div class="card-header card-header-primary">
											<h4 class="card-title">Soal No {{ $i+1 }}</h4>
										</div><br>
										<label class="bmd-label-floating">Soal</label>
										<div class="form-group">
											<textarea rows="3" type="text" class="form-control" name="soal[]" required>{{ $pertanyaan2[$i]->pertanyaan}}</textarea>
										</div>
										<div class="form-group">
											<label class="bmd-label-floating">Pilihan A</label>
											<input type="text" class="form-control" name="pilgan1[]" required value="{{ $pertanyaan2[$i]->pilgan()->first()->A }}">
										</div>
										<div class="form-group">
											<label class="bmd-label-floating">Pilihan B</label>
											<input type="text" class="form-control" name="pilgan2[]" required value="{{ $pertanyaan2[$i]->pilgan()->first()->B }}">
										</div>
										<div class="form-group">
											<label class="bmd-label-floating">Pilihan C</label>
											<input type="text" class="form-control" name="pilgan3[]" required value="{{ $pertanyaan2[$i]->pilgan()->first()->C }}">
										</div>
										<div class="form-group">
											<label class="bmd-label-floating">Pilihan D</label>
											<input type="text" class="form-control" name="pilgan4[]" required value="{{ $pertanyaan2[$i]->pilgan()->first()->D }}">
										</div>
										<div class="form-group">
											<label class="bmd-label-floating">Kunci Jawaban:</label>
											<input type="text" class="form-control" name="kunciJawaban[]" required value="{{ $pertanyaan2[$i]->kunciJawaban}}">
										</div>
									</div>
								</div>
								<br><br><br><hr>
								@endfor
							</div>
							<button type="submit" class="btn btn-primary pull-right">Update</button>
							<div class="clearfix"></div>
						</form>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('extrascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
